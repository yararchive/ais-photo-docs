var Config = {
  db: {
    AIS: {
      server: "SQLSERVER",
      name: "SQLDBNAME",
      user: "SQLUSERNAME",
      pass: "SQLUSERPASS",
      archive_id: 2,
      photodocs_fund_id: 6,
      photodocs_catalog_id: 9
    },
    max_reconnects: 0, // ������� ��� ���������� ������� ����������� � ��. 0 - ����� ������� �� ���������.
    reconnect_interval: 1, // � ��������,
    // ������������ ���������� �������, ������������� � �� �� ���
    part_size: 1000
  },
  log: {
    level: ["info", "error", "critical"],
    file: ".\\log\\ais-photo-doc.log",
    unique_name: true
  },
  storage: {
    units_root: ["\\\\PATH\\TO\\STORAGE\\u\\", "\\\\PATH2\\TO\\STORAGE\\"], // ���� �� ����� ����������
    previews_root: ["\\\\PATH\\TO\\STORAGE\\p\\", "\\\\PATH2\\TO\\STORAGE\\"], // ���� �� ����� ����������
    extensions: ["jpg", "png"]
  }
};