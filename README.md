# Версии совместимого ПО

- Архивный фонд 5.0.2

- АИС Архив 2.19.0.0

- Работает только на windows (гарантированно на windows server 2008 R2)

# Настройка соединения с БД

В файле includes/Config.js в секции db.

# Как запустить

В командной строке или powershell перейти в каталог проекта и выполнить

```bash
cscript ais-photo-doc.wsf
```

Двойным кликом лучше не запускать, алерты замучают.

В скрипте есть возможность прикреплять образы документов к фотодокументам и изобажениям предпросмотра. Источников образов может быть несколько. Приоритет загрузки электронных образов идет в порядке перечисления источников. Т.е., если одно и то же дело присутствует в двух хранилищах, образы в БД АИС попадут только из первого указанного в файле Config.js.